#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Raspitangle - IOTA Tangle implementation on RaspberryPi.
#  2019, Aug.
#  
#  Copyright 2019  Daniel Muller <daniel@conexum.com.br>
#                  Douglas Viegas <douglasvviegas@gmail.com>
#                  
#  Conexum Sistemas Computacionais Inteligentes
#  
#  htpps://www.conexum.com.br
#
#  Raspitangle is free software; you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

import paho.mqtt.client as paho
import json
import random
import time
import sys
import Adafruit_DHT

    
def get_data():
	humidity, temperature = Adafruit_DHT.read_retry(22, 4)
	data = {'humidity'   : {'value': humidity, 'unit':'%RH'},
			'temperature': {'value': temperature, 'unit':'C'}}
	return data
            
def get_timestamp():
    return time.time()
            
if __name__ == "__main__":
	broker = 'localhost'
	port = 1883
	client = paho.Client()
	client.connect(broker,port)
	while True:
		data = get_data()
		timestamp = get_timestamp()
		dados = {'timestamp': timestamp,'data': data}
		print(dados)
		dadosprep = json.dumps(dados, separators=(',',':'))
		print(dadosprep)
		ret = client.publish('sensors/data',dadosprep)
		print("Enviando para mqtt broker")
		print(ret)
		time.sleep(30.)
